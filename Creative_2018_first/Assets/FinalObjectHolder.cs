﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinalObjectHolder : MonoBehaviour {

    public float timeBeforeEndingLevel;

    private TriggerDetectPlayer trigger;
    private GameObject player;
    private Inventory inv;
    private FinishLevel fl;


	void Start ()
    {
        trigger = GetComponent<TriggerDetectPlayer>();
        player = FindObjectOfType<Inventory>().gameObject;
        inv = player.GetComponent<Inventory>();
        fl = FindObjectOfType<FinishLevel>().gameObject.GetComponent<FinishLevel>();
    }

    void Update ()
    {
		if(trigger.playerInLocalTrigger && Input.GetKey(KeyCode.E) && inv.haveFinalObject)
        {
            StartCoroutine(EndLevel());
        }
	}

    IEnumerator EndLevel()
    {
        yield return null;

        yield return new WaitForSeconds(timeBeforeEndingLevel);

        fl.EndLevel();
    }
}
