﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelState : MonoBehaviour {

    public static LevelState instance = null;

    public bool isFirstLevelFinish;
    public bool isSecondLevelFinish;
    public bool isThirdLevelFinish;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    void Start ()
    {
		
	}
	
	void Update () {
		
	}
}
