﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_Shield : MonoBehaviour {

    public GameObject UICode;

	void Start ()
    {
        UICode.SetActive(false);
    }
	
	void Update ()
    {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            UICode.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            UICode.SetActive(false);
        }
    }
}
