﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_NPC : MonoBehaviour {


    private TriggerDetectPlayer trigger;
    private DialogueTrigger dialogue_trigger;
    

    private void Start()
    {
        trigger = GetComponent<TriggerDetectPlayer>();
        dialogue_trigger = GetComponent<DialogueTrigger>();
    }

    void Update ()
    {
		if(trigger.playerInLocalTrigger && Input.GetKeyDown(KeyCode.E))
        {
            
            dialogue_trigger.TriggerDialogue();
        }
	}
}
