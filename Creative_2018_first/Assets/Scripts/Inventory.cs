﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public Image signDisplay;
    public Image signDisplayFinalImage;

    public GameObject objectPrefab00;
    public GameObject objectPrefab01;
    public GameObject objectPrefab02;
    public GameObject objectPrefab03;
    public GameObject objectPrefab04;
    public GameObject objectPrefab05;
    public GameObject objectPrefab06;
    public GameObject objectPrefab07;

    [HideInInspector] public bool haveObject;
    [HideInInspector] public bool haveFinalObject;

    private GameObject currentObj;

    private GameObject pickup;

    public void GrabObject(int pickupIndex, GameObject localObj, GameObject pickupPrefab, Sprite pickupImage)
    {
        if (pickupIndex == 0) pickup = objectPrefab00;
        else if (pickupIndex == 1) pickup = objectPrefab01;
        else if (pickupIndex == 2) pickup = objectPrefab02;
        else if (pickupIndex == 3) pickup = objectPrefab03;
        else if (pickupIndex == 4) pickup = objectPrefab04;
        else if (pickupIndex == 5) pickup = objectPrefab05;
        else if (pickupIndex == 6) pickup = objectPrefab06;
        else if (pickupIndex == 7) pickup = objectPrefab07;

        signDisplay.enabled = true;

        currentObj = pickup;
        currentObj.transform.GetComponentInChildren<Trigger_Pickup>().isInHolder = false;

        signDisplay.sprite = pickupImage;

        Destroy(localObj);
    }

    public void ReplaceObject(int pickupIndex, GameObject localObj, GameObject pickupPrefab, Sprite pickupImage, Transform pos)
    {
        Vector3 _pos = pos.position;
        Instantiate(pickup, _pos, Quaternion.EulerAngles(0, 45f, 0));

        Destroy(localObj);

        if (pickupIndex == 0) pickup = objectPrefab00;
        else if (pickupIndex == 1) pickup = objectPrefab01;
        else if (pickupIndex == 2) pickup = objectPrefab02;
        else if (pickupIndex == 3) pickup = objectPrefab03;
        else if (pickupIndex == 4) pickup = objectPrefab04;
        else if (pickupIndex == 5) pickup = objectPrefab05;
        else if (pickupIndex == 6) pickup = objectPrefab06;
        else if (pickupIndex == 7) pickup = objectPrefab07;

        currentObj = pickup;
        currentObj.transform.GetComponentInChildren<Trigger_Pickup>().isInHolder = false;

        signDisplay.sprite = pickupImage;
    }

    public void PutObject(Transform pos, GameObject obj)
    {
        Trigger_Pickup holder = obj.GetComponent<Trigger_Pickup>();

        holder.currentHoldObject = currentObj.transform.GetComponentInChildren<Trigger_Pickup>().gameObject;
        currentObj.transform.GetComponentInChildren<Trigger_Pickup>().isInHolder = true;
        holder.CheckObject();

        Vector3 _pos = pos.position;
        Instantiate(pickup, _pos, Quaternion.Euler(0, -90f, 0));

        currentObj = null;
        signDisplay.sprite = null;
        signDisplay.enabled = false;
    }

    public void GrabFinalObject(Sprite finalSprite)
    {
        haveFinalObject = true;
        signDisplayFinalImage.enabled = true;
        signDisplayFinalImage.sprite = finalSprite;
    }
}