﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDetectPlayer : MonoBehaviour {

    [HideInInspector] public bool playerInLocalTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            other.GetComponent<Interaction>().canInteract = true;
            playerInLocalTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            TurnSignOff(other.gameObject);
        }
    }

    public void TurnSignOff(GameObject other)
    {
        other.GetComponent<Interaction>().canInteract = false;
        playerInLocalTrigger = false;
    }
}
