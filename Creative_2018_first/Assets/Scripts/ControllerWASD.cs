﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerWASD : MonoBehaviour {

    [Header("Controller Parameters")]
    public float moveSpeed;
    public float rotateSpeed;
    Bank laBank;

    Animator myAnim;
    
    private Rigidbody rb;

	void Start ()
    {
        myAnim = GetComponentInChildren<Animator>();
        laBank = FindObjectOfType<Bank>();
        rb = GetComponent<Rigidbody>();
	}
	
	void Update ()
    {
        //MOVE PLAYER
        if (Input.GetKey(KeyCode.W)) //Input.GetAxis("Horizontal") > 0
        {
            rb.velocity = transform.forward * moveSpeed * 4f * Time.deltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.velocity = transform.forward * -moveSpeed * 4f * Time.deltaTime;
        }
        else rb.velocity = Vector3.zero;


        //ROTATE PLAYER
        if(Input.GetKey(KeyCode.A))
        {
            transform.Rotate(new Vector3(0, -rotateSpeed * 8f * Time.deltaTime, 0));
        }
        else if(Input.GetKey(KeyCode.D))
        {
            transform.Rotate(new Vector3(0, rotateSpeed * 8f * Time.deltaTime, 0));
        }

        if (rb.velocity != Vector3.zero && transform.name != "fantomeJoueur")
        {
            myAnim.SetBool("Walk", true);
        }
        else if(transform.name != "fantomeJoueur")
        {
            myAnim.SetBool("Walk", false);
        }
    }

}