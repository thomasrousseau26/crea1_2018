﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckLevelState : MonoBehaviour {

    public int levelNumber;
	
	void Update ()
    {
		if(levelNumber == 1 && LevelState.instance.isFirstLevelFinish)
        {
            Destroy(gameObject);
        }
        else if (levelNumber == 2 && LevelState.instance.isSecondLevelFinish)
        {
            Destroy(gameObject);
        }
        else if (levelNumber == 3 && LevelState.instance.isThirdLevelFinish)
        {
            Destroy(gameObject);
        }
    }
}
