﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

    public List<FloorTile> tiles;
    public string tileOrder;

    public Animator triggeredAnimation;

    public string actualOrder;

    private int counter;

	
	void Update ()
    {
        CheckTilesState();
    }

    private void CheckTilesState()
    {
        counter = 0;

        for (int i = 0; i < tiles.Count; i++)
        {
            if (tiles[i].isActive) counter++;
        }

        if(counter == tiles.Count)
        {
            CheckTileOrder();
        }
    }

    private void CheckTileOrder()
    {
        if (actualOrder == tileOrder)
        {
            //OUTPUT
            triggeredAnimation.SetBool("isActivated", true);
        }
        else
        {
            StartCoroutine(TileDeactivateFeedback());
            actualOrder = "";
        }
    }

    IEnumerator TileDeactivateFeedback()
    {
        yield return new WaitForSeconds(1.5f);

        for (int i = 0; i < tiles.Count; i++)
        {
            tiles[i].DeactivateFeedback();
        }
    }
}