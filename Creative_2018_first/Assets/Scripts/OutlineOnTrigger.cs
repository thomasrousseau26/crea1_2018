﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutlineOnTrigger : MonoBehaviour {

    private GameObject objectToHighlight;

    private void Start()
    {
        objectToHighlight = transform.parent.gameObject;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            gameObject.GetComponent<highlightSelf>().isSelected = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            gameObject.GetComponent<highlightSelf>().isSelected = false;
        }
    }
}