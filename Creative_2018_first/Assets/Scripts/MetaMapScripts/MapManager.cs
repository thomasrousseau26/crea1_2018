﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour {

    GameObject[] maMetaMap;
    Vector3[] maMetaMapTrans;
    public GameObject maSelect;
    public float speedDestroy;
	// Use this for initialization
	void Start () {
        maMetaMap = GameObject.FindGameObjectsWithTag(Containts.tagMiniMap);
        maMetaMapTrans = new Vector3[maMetaMap.Length];
        for (int i = 0; i < maMetaMap.Length; i++) {
            maMetaMapTrans[i] = maMetaMap[i].transform.position;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space)) {
            StartCoroutine(DisparitionMap());
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            MyReset();
        }

    }

    IEnumerator DisparitionMap() {

        foreach (GameObject partMap in maMetaMap) {
            yield return new WaitForSeconds(speedDestroy);
            if (partMap != maSelect) {
                partMap.GetComponent<Rigidbody>().useGravity = true;
                partMap.GetComponent<Rigidbody>().isKinematic = false;
                partMap.GetComponent<Rigidbody>().AddTorque(new Vector3(Random.Range(-800, 800), Random.Range(-800, 800), Random.Range(-800, 800)), ForceMode.Impulse);

            }
        }

        yield return null;
    }

    public void MyReset()
    {
        Debug.Log("Reset");
        for (int i = 0; i < maMetaMap.Length; i++)
        {
            Debug.Log("ResetIn");
            maMetaMap[i].transform.position = maMetaMapTrans[i];
            maMetaMap[i].transform.rotation = new Quaternion(0, 0, 0, 0) ;
            maMetaMap[i].GetComponent<Rigidbody>().isKinematic = true;
            maMetaMap[i].GetComponent<Rigidbody>().useGravity = false;
        }
    }

}
