﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LightRebond : MonoBehaviour {

    RaycastHit hit;

    LineRenderer myLine;

    public bool stat;
    bool cibleOk = false;
    public Vector3 myAngle = Vector3.zero;
    GameObject maCible;
    bool aTouche;
    public Transform parents;

    public bool enTournant = false;
    public bool imACible = false;

    

    // Use this for initialization
    void Start () {
        enTournant = false;
        myLine = GetComponent<LineRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        /*Debug.DrawRay(transform.position, transform.right, Color.yellow);
        Debug.DrawRay(transform.position, -transform.right, Color.blue);*/


       

        if (stat) {
            myAngle = transform.forward; 
            // myAngle = transform.forward;

            Debug.DrawRay(transform.position, transform.forward, Color.red);
            if (Physics.Raycast(transform.position, myAngle, out hit, Mathf.Infinity)){
                //if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Hit");

                if (hit.collider.tag == Containts.tagMirror)
                {
                    //if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log(hit.collider.name + "Hit mirror");

                    maCible = hit.collider.gameObject;
                    CalculeAngle(hit.collider.gameObject);
                    hit.collider.gameObject.GetComponent<LightRebond>().parents = this.gameObject.transform;
                    
                    //if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Angle" + hit.collider.transform.eulerAngles.y);
                }
                if(hit.collider.gameObject != maCible && maCible != null) {
                    maCible.GetComponent<LightRebond>().cibleOk = false;

                }
            }
        }

        if(cibleOk && !stat)
        {
            if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Cible ok " + transform.name);

            myLine.enabled = true;
            //JeTireMonRay(myAngle);   
            if (Physics.Raycast(transform.position, myAngle, out hit, Mathf.Infinity))
            {
                if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Raycast " + transform.name);

                //if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Hit");
                Debug.DrawRay(transform.position, myAngle * 1000, Color.black);

                if (hit.collider.tag == Containts.tagMirror && hit.transform != parents && !enTournant)
                {
                    if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log(hit.collider.name + " est touché");
                    if (maCible == null)
                    {
                        if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Raycast + Non Cible" + transform.name);
                        maCible = hit.collider.gameObject;
                    }
                    else if (!enTournant && hit.collider.gameObject.GetComponent<LightRebond>().imACible == false)
                    {
                        if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Raycast + Cible" + transform.name);
                        maCible.GetComponent<LightRebond>().imACible = false;
                        maCible.GetComponent<LightRebond>().cibleOk = false;
                        maCible = hit.collider.gameObject;
                        maCible.GetComponent<LightRebond>().cibleOk = true;
                        maCible.GetComponent<LightRebond>().imACible = true;
                    }
                    CalculeAngle(maCible);
                    maCible.GetComponent<LightRebond>().parents = this.gameObject.transform;
                    aTouche = true;
                }
                else if (maCible != null)
                {
                    if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("No Mirror " + transform.name);

                    //if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log(transform.name);
                    maCible.GetComponent<LightRebond>().imACible = false;
                    maCible.GetComponent<LightRebond>().cibleOk = false;
                    maCible = null;
                }

                if (hit.collider.tag == Containts.tagSarco && !enTournant) {

                    SceneManager.LoadScene("Main");

                }

                myLine.SetPosition(0, transform.position);
                myLine.SetPosition(1, hit.point);

            }
            else
            {
                if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("No Raycast " + transform.name);

                if (aTouche)
                {
                    if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("No Raycast + atouché" + transform.name);

                    maCible.GetComponent<LightRebond>().cibleOk = false;
                    maCible.GetComponent<LightRebond>().maCible = null;

                    aTouche = false;
                }
                myLine.SetPosition(0, transform.position);
                myLine.SetPosition(1, myAngle * 100);
            }

        }
        if (!cibleOk && myLine != null)
        {//QTTENTION

            if (transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("cibleOk no" + transform.name);


            myLine.enabled = false;
            if (maCible != null)
            {
                maCible.GetComponent<LightRebond>().cibleOk = false;
                maCible.GetComponent<LightRebond>().imACible = false;
            }


        }

        if (maCible != null) {
            maCible.GetComponent<LightRebond>().cibleOk = true;
            maCible.GetComponent<LightRebond>().imACible = true;
            
            CalculeAngle(maCible);
            maCible.GetComponent<LightRebond>().parents = this.gameObject.transform;
            if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("no cible" + transform.name);

        }


        

        if (!imACible && maCible != null) {
            myLine.enabled = false;
            maCible.GetComponent<LightRebond>().cibleOk = false;
        }

    }

   /* public void JeTireMonRay(Vector3 angle) {

        

        if (Physics.Raycast(transform.position, angle, out hit, Mathf.Infinity))
        {
            //if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log("Hit");

            if (hit.collider.tag == Containts.tagMirror && hit.transform != parents && !enTournant)
            {
                Debug.DrawRay(transform.position, angle * 1000, Color.black);
                //if(transform.name == "Cube 5" || transform.name == "Cube 4") Debug.Log(hit.collider.name + "Hit mirror");
                if (maCible == null)
                {
                    maCible = hit.collider.gameObject;
                }
                else if(!enTournant && hit.collider.gameObject.GetComponent<LightRebond>().imACible == false)
                {
                    maCible.GetComponentInChildren<LightRebond>().imACible = false;
                    maCible.GetComponentInChildren<LightRebond>().cibleOk = false;
                    maCible = hit.collider.gameObject;
                    maCible.GetComponentInChildren<LightRebond>().cibleOk = true;
                    maCible.GetComponentInChildren<LightRebond>().imACible = true;
                }
                CalculeAngle(hit.collider.gameObject);
                hit.collider.gameObject.GetComponentInChildren<LightRebond>().parents = this.gameObject.transform;
            }
            if (hit.collider.gameObject != maCible && maCible != null)
            {
                maCible.GetComponentInChildren<LightRebond>().cibleOk = false;
            }
            aTouche = true;
            myLine.SetPosition(0, transform.position);
            myLine.SetPosition(1, hit.point);

        }
        else {
            if (aTouche) {

                maCible.GetComponent<LightRebond>().cibleOk = false;
                maCible.GetComponent<LineRenderer>().enabled= false;


                aTouche = false;
            }
            myLine.SetPosition(0, transform.position);
            myLine.SetPosition(1, myAngle*100);
        }
        

    }*/

    void CalculeAngle(GameObject ye) {
 
        ye.GetComponent<LightRebond>().myAngle = Vector3.Reflect(myAngle, ye.transform.forward);

        ye.GetComponent<LightRebond>().cibleOk = true;
        ye.GetComponent<LightRebond>().imACible = true;


    }


}
