﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToClick : MonoBehaviour {

    RaycastHit hit;

    Vector3 m_destination;

    public GameObject m_player;
    MapManager monManger;
    // Use this for initialization
    void Start () {
        monManger = FindObjectOfType<MapManager>();
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //On regarde où clic le joueur
        if (Physics.Raycast(ray, out hit, 100))
        {
            Debug.DrawLine(ray.origin, hit.point, Color.green);
            if (Input.GetMouseButtonDown(0))
            {
                //Action de déplacement
                m_destination = hit.transform.position;
                monManger.maSelect = hit.collider.gameObject.transform.parent.gameObject;
            }
        }
    }

    private void FixedUpdate()
    {
        if (m_destination != Vector3.zero) {
            m_player.transform.Translate(-(m_player.transform.position - m_destination).normalized * Time.deltaTime);
        }
    }

}
