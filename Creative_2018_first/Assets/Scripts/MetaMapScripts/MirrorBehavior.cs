﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorBehavior : MonoBehaviour {

    public GameObject leMirror;
    int i = 0;
    protected float angle;
    bool versDroite = true;
    bool apu = false;
	// Use this for initialization
	void Start () {
        angle = transform.rotation.eulerAngles.y;
    }
	
	// Update is called once per frame
	void Update () {
        
        /*if (Input.GetKeyDown(KeyCode.A))
        {
            //apu = true;
           StartCoroutine(Jetouren());
            //TurnMySon();
        }*/
       



    }

    private void OnTriggerStay(Collider other)
    {
        if(other.tag == Containts.tagPlayer && Input.GetKeyDown(KeyCode.E) && !apu)
        {
            apu = !apu;
            StartCoroutine(Jetouren());
        }
    }

    IEnumerator Jetouren() {

        leMirror.GetComponentInChildren<LightRebond>().enTournant = true;
        
        var angleMax = angle + 22.5f;
        //Debug.Log(angleMax);
        while (i < 10000 && angle<angleMax) {

            leMirror.transform.Rotate(0, 100 * Time.fixedDeltaTime, 0);
            angle += 100 * Time.fixedDeltaTime;
            yield return null;
            i++;
        }
        angle = angleMax;
        leMirror.transform.rotation = Quaternion.Euler(0, angleMax, 0);

        leMirror.GetComponentInChildren<LightRebond>().enTournant = false;
        apu = !apu;
        //StopAllCoroutines();
    }
}
