﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTile : MonoBehaviour {

    public int index;
    public Material highlightMat;
    public GameObject maTile;
    [HideInInspector] public bool isActive;

    private Material originMat;

    private Animator anim;
    private MeshRenderer mr;
    private TileManager tm;

    

	void Start ()
    {
        anim = GetComponent<Animator>();
        mr = maTile.GetComponent<MeshRenderer>();
        tm = FindObjectOfType<TileManager>();

        originMat = mr.material;
	}

    private void OnTriggerEnter(Collider other)
    {
        if ((other.gameObject.CompareTag("Player")) && !isActive)
        {
            isActive = true;
            ActivateFeedback();

            tm.actualOrder = tm.actualOrder + index.ToString();
        }
    }

    private void ActivateFeedback()
    {
        anim.SetBool("isActivated", true);
        mr.material = highlightMat;
    }

    public void DeactivateFeedback()
    {
        isActive = false;
        anim.SetBool("isActivated", false);
        mr.material = originMat;
    }


    //private void OnTriggerExit(Collider other)
    //{
    //    if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Rock"))
    //    {
    //        isActive = false;
    //        anim.SetBool("isActivated", false);
    //    }
    //}
}