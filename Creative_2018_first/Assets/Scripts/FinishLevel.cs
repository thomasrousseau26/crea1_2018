﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLevel : MonoBehaviour {

    public int levelNumber;
    public Animator fadeImageAnim;


	public void EndLevel()
    {
        StartCoroutine(End());
    }

    IEnumerator End()
    {
        fadeImageAnim.SetBool("isActivated", true);

        yield return new WaitForSeconds(1f);

        if (levelNumber == 1)
        {
            LevelState.instance.isFirstLevelFinish = true;
            SceneManager.LoadScene("Main");
        }
        else if (levelNumber == 2)
        {
            LevelState.instance.isSecondLevelFinish = true;
            SceneManager.LoadScene("Main");
        }
        else if (levelNumber == 3)
        {
            LevelState.instance.isThirdLevelFinish = true;
            SceneManager.LoadScene("Main");
        }
    }
}