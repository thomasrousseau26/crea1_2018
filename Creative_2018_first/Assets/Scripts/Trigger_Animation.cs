﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger_Animation : MonoBehaviour {

    public bool moreThanOneInteraction;
    public Animator objectAnim;

    private TriggerDetectPlayer trigger;

    private void Start()
    {
        trigger = GetComponent<TriggerDetectPlayer>();
    }

    void Update ()
    {
		if(trigger.playerInLocalTrigger && Input.GetKeyDown(KeyCode.E) && !moreThanOneInteraction)
        {
            objectAnim.SetBool("isFirstInteraction", true);
            trigger.TurnSignOff(FindObjectOfType<Interaction>().gameObject);
            gameObject.SetActive(false);
        }
	}
}
