﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour {

    public GameObject interactSign;

    [HideInInspector] public bool canInteract;

	void Start ()
    {
		
	}
	
	void Update ()
    {
        if (canInteract) interactSign.SetActive(true);
        else if (!canInteract) interactSign.SetActive(false);
	}
}
