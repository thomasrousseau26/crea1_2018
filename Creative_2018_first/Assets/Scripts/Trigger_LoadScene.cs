﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Trigger_LoadScene : MonoBehaviour
{
    public string sceneName;
    public Animator fadeImageAnim;

    private TriggerDetectPlayer trigger;

    private void Start()
    {
        trigger = GetComponent<TriggerDetectPlayer>();
    }

    void Update()
    {
        if (trigger.playerInLocalTrigger && Input.GetKeyDown(KeyCode.E))
        {
            StartCoroutine(Fade());
        }
    }

    IEnumerator Fade()
    {
        fadeImageAnim.SetBool("isActivated", true);

        yield return new WaitForSeconds(.8f);

        SceneManager.LoadScene(sceneName);
    }
}