﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trigger_Pickup : MonoBehaviour
{
    public bool isEmpty;

    public GameObject objPrefab;
    public Sprite objImage;
    public Transform pos;

    public int index;

    private GameObject pickupObject;
    private GameObject player;
    private TriggerDetectPlayer trigger;

    private Inventory inv;
    [HideInInspector] public bool goodObj;

    [HideInInspector] public GameObject currentHoldObject;

    [HideInInspector] public bool isInHolder;

    private void Start()
    {
        pickupObject = transform.parent.gameObject;
        trigger = GetComponent<TriggerDetectPlayer>();
        player = FindObjectOfType<Inventory>().gameObject;
        inv = player.gameObject.GetComponent<Inventory>();
    }

    void Update()
    {
        if (trigger.playerInLocalTrigger && Input.GetKeyDown(KeyCode.E) && !inv.haveObject && !isEmpty)
        {
            inv.GrabObject(index, transform.parent.gameObject, objPrefab, objImage);
            trigger.TurnSignOff(FindObjectOfType<Interaction>().gameObject);
            inv.haveObject = true;
        }
        else if (trigger.playerInLocalTrigger && Input.GetKeyDown(KeyCode.E) && inv.haveObject && !isEmpty)
        {
            inv.ReplaceObject(index, transform.parent.gameObject, objPrefab, objImage, pos);
        }

        if (trigger.playerInLocalTrigger && Input.GetKeyDown(KeyCode.E) && inv.haveObject && isEmpty)
        {
            inv.PutObject(pos, transform.gameObject);
            inv.haveObject = false;
        }

        CheckObject();
    }

    public void CheckObject()
    {
        if (isEmpty) //IF I HAVE AN OBJECT AND I AM A HOLDER
        {
            if (currentHoldObject != null)
            {
                if (currentHoldObject.GetComponent<Trigger_Pickup>().index == index && currentHoldObject.GetComponent<Trigger_Pickup>().isInHolder)
                {
                    goodObj = true;
                }
                else goodObj = false;
            }
        }
    }
}