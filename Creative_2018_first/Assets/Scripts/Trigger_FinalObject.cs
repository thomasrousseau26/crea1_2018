﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Trigger_FinalObject : MonoBehaviour
{
    public bool isEmpty;

    public Sprite objImage;

    private GameObject player;
    private TriggerDetectPlayer trigger;
    private Inventory inv;

    private void Start()
    {
        trigger = GetComponent<TriggerDetectPlayer>();
        player = FindObjectOfType<Inventory>().gameObject;
        inv = player.gameObject.GetComponent<Inventory>();
    }

    void Update()
    {
        if (trigger.playerInLocalTrigger && Input.GetKeyDown(KeyCode.E))
        {
            inv.GrabFinalObject(objImage);
            trigger.TurnSignOff(FindObjectOfType<Interaction>().gameObject);
            Destroy(transform.parent.gameObject);
        }
    }
}