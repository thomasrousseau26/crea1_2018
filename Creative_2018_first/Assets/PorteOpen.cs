﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PorteOpen : MonoBehaviour {

    public int jeVaisOU;
    bool okiPrendMoi;
    Bank maBank;
    GameObject player;
    /*public*/
    GameObject maRefUI;
    public string nomdeclef;

    private void Start()
    {
        maRefUI = GameObject.Find(nomdeclef);
        maBank = FindObjectOfType<Bank>();
    }

    private void Update()
    {
        if (okiPrendMoi && Input.GetKeyDown(KeyCode.E) && maBank.mesObjets[0] != null && maBank.mesObjets[1] != null && maBank.mesObjets[2] != null)
        {

            Application.Quit();
           
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Containts.tagPlayer)
        {
            player = other.gameObject;
            okiPrendMoi = true;
            Debug.Log("enter");
            other.GetComponent<Interaction>().canInteract = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Containts.tagPlayer)
        {
            okiPrendMoi = false;
            Debug.Log("exit");
            other.GetComponent<Interaction>().canInteract = false;
        }
    }
}
