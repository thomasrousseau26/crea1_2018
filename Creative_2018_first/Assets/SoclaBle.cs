﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoclaBle : MonoBehaviour {

    bool okiPrendMoi;
    Bank maBank;
    GameObject player;
    public int jePrendQui;
    /*public*/ GameObject maRefUI;
    public string nomdeclef;

    private void Start()
    {
        maRefUI = GameObject.Find(nomdeclef);
        maBank = FindObjectOfType<Bank>();
    }

    private void Update()
    {
        if (okiPrendMoi && Input.GetKeyDown(KeyCode.E) && maBank.mesObjets[jePrendQui] != null)
        {
            
            maBank.mesObjets[jePrendQui].transform.position = transform.position + Vector3.up;
            maBank.mesObjets[jePrendQui].SetActive(true);
            maBank.mesObjets[jePrendQui] = null;
            player.GetComponent<Interaction>().canInteract = false;
            maRefUI.SetActive(false);


        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Containts.tagPlayer)
        {
            player = other.gameObject;
            okiPrendMoi = true;
            Debug.Log("enter");
            other.GetComponent<Interaction>().canInteract = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Containts.tagPlayer)
        {
            okiPrendMoi = false;
            Debug.Log("exit");
            other.GetComponent<Interaction>().canInteract = false;
        }
    }
}
