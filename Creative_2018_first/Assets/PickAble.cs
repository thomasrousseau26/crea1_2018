﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickAble : MonoBehaviour {

    public int jeVaisOU;
    bool okiPrendMoi;
    Bank maBank;
    GameObject player;
    /*public*/ GameObject maRefUI;
    public string nomdeclef;

    private void Start()
    {
        maRefUI = GameObject.Find(nomdeclef);
        maBank = FindObjectOfType<Bank>();
    }

    private void Update()
    {
        if (okiPrendMoi && Input.GetKeyDown(KeyCode.E)) {
            
            maBank.mesObjets[jeVaisOU] = this.gameObject;
            gameObject.SetActive(false);
            player.GetComponent<Interaction>().canInteract = false;
            maRefUI.SetActive(true);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == Containts.tagPlayer)
        {
            player = other.gameObject;
            okiPrendMoi = true;
            Debug.Log("enter");
            other.GetComponent<Interaction>().canInteract = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == Containts.tagPlayer)
        {
            okiPrendMoi = false;
            Debug.Log("exit");
            other.GetComponent<Interaction>().canInteract = false;
        }
    }

}
