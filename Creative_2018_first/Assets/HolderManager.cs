﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolderManager : MonoBehaviour
{

    public List<Trigger_Pickup> holders;
    public Animator triggeredAnim;

    private FinishLevel fl;
    private int counter;

    private void Start()
    {
        fl = FindObjectOfType<FinishLevel>();
    }

    void Update()
    {
        counter = 0;

        for (int i = 0; i < holders.Count; i++)
        {
            if (holders[i].goodObj)
            {
                counter++;
            }
        }

        if (counter == holders.Count)
        {
            triggeredAnim.SetBool("isActivated", true);
        }
    }

    IEnumerator FinalFeedback()
    {
        yield return null;

        yield return new WaitForSeconds(3f);

        fl.EndLevel();
    }
}